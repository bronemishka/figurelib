namespace FigureTest
{
    public class Tests
    {
        private IFigure? figure;
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestTriangle1()
        {
            try
            {
                figure = new Triangle(1, 1, 1215);
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void TestTriangle2()
        {
            try
            {
                figure = new Triangle(-3, -4, -5);
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void TestTriangle3()
        {
            try
            {
                figure = new Triangle(3, 4, 5);
            }
            catch (ArgumentException)
            {
                Assert.Fail();
            }
            Assert.That(((Triangle)figure).IsRightTriangle && figure.FindArea() == 6);
        }
        [Test]
        public void TestTriangle4()
        {
            try
            {
                figure = new Triangle(1, 2, 3);
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void TestTriangle5()
        {
            try
            {
                figure = new Triangle(1, 1, 2);
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void TestCircle1()
        {
            figure = new Circle(1);
            Assert.That(figure.FindArea() == Math.PI);
        }
        [Test]
        public void TestCircle2()
        {
            try
            {
                figure = new Circle(-1);
            }
            catch (ArgumentException)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
    }
}