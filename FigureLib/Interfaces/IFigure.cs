﻿namespace FigureLib.Interfaces
{
    public interface IFigure
    {
        public double FindPerimeter() => throw new NotImplementedException("FindPerimeter not implemented");
        public double FindArea() => throw new NotImplementedException("FindArea not implemented");
        public bool IsValid() => throw new NotImplementedException("IsValid not implemented");
    }
}
