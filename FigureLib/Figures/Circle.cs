﻿using FigureLib.Interfaces;

namespace FigureLib.Figures
{
    public class Circle : IFigure
    {
        public readonly double Radius;
        public double FindPerimeter() => 2 * Math.PI * Radius;
        public double FindArea() => Math.PI * Math.Pow(Radius, 2);
        public bool IsValid() => Radius > 0;
        public Circle(double r)
        {
            Radius = r;
            if (!IsValid()) throw new ArgumentException("Impossible circle");
        }
    }
}
