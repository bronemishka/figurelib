﻿using FigureLib.Interfaces;

namespace FigureLib.Figures
{
    public class Triangle : IFigure
    {
        public readonly double SideA;
        public readonly double SideB;
        public readonly double SideC;
        public readonly bool IsRightTriangle;
        public double FindArea()
        {
            var p = FindPerimeter()/2;
            return Math.Sqrt(p * (p - SideA) * (p - SideB) * (p - SideC));
        }
        public double FindPerimeter() => SideA + SideB + SideC;
        public bool IsValid() => SideA > 0 && SideB > 0 && SideC > 0 && (SideA + SideB > SideC && SideB + SideC > SideA && SideA + SideC > SideB);

        private bool CheckIsRightTriangle()
        {
            if(Math.Sqrt(Math.Pow(SideA, 2) + Math.Pow(SideB, 2)) == SideC) return true;
            if (Math.Sqrt(Math.Pow(SideC, 2) + Math.Pow(SideA, 2)) == SideB) return true;
            return Math.Sqrt(Math.Pow(SideB, 2) + Math.Pow(SideC, 2)) == SideA;
        }
        public Triangle(double a, double b, double c)
        {
            SideA = a;
            SideB = b;
            SideC = c;
            if (!IsValid()) throw new ArgumentException("Impossible triangle");
            IsRightTriangle = CheckIsRightTriangle();
        }
    }
}